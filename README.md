# PrintServerSimulator

The goal of this was to create a small executable that opens up a port on the localhost, in order to simulate the **GIS Print Server**. This way we can test the connectivity of the ``PrintController`` class of the [main 3D Metal Printer project](https://bitbucket.org/adamalpaca/printer3d_v2/src/master/).

To open up a port you want:

    <path>\PrintServerSimulator.exe <port>